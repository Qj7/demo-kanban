Rails.application.routes.draw do
  devise_for :users
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  devise_scope :user do
    get 'login', to: 'devise/sessions#new'
    root to: "devise/sessions#new"
  end

  get 'users/main' => 'users#index'
  get 'user' => 'users#show'
  get 'users/all' => 'users#all'
  post 'update_user' => 'users#update'

  get 'create_issue' => 'issues#create'
  post 'create_issue' => 'issues#create'

  #get 'users' => 'users#index'



  get 'issue' => 'issues#show'



  get 'to_work' => 'issues#to_work'
  get 'to_later' => 'issues#to_later'
  get 'to_cancel' => 'issues#to_cancel'
  get 'to_close' => 'issues#to_close'

  get 'supports/main' => 'supports#index'
  get 'supports/issues' => 'supports#issues'
  get 'supports/dashboard' => 'supports#dashboard'

  get 'supports/update_issue_status' => 'supports#update_issue_status'
  get 'supports/update_issue_priority' => 'supports#update_issue_priority'
  get 'supports/update_issue_classification' => 'supports#update_issue_classification'
  get 'find_issue' => 'supports#find_issue'

  get 'supports/show_panel' => 'supports#show_panel'
  get 'supports/show_comment' => 'supports#show_comment'
  get 'supports/comments' => 'supports#comments'

  get 'admins/categories' => 'admins#categories'
  get 'admins/response_time' => 'admins#response_time'
  post 'set_response_time' => 'admins#set_response_time'

  post 'categories_create' => 'categories#create'
  post 'categories_update' => 'categories#update'
  get 'categories_destroy' => 'categories#destroy'

  get 'create_comment' => 'comments#create_comment'
  post 'create_comment' => 'comments#create_comment'
  get 'comments_readed' => 'comments#readed'
  get 'count_unread' => 'comments#count_unread'
  get 'count_specific_unread' => 'comments#count_specific_unread'

  get 'test' => 'supports#test'

  post 'create_attachment' => 'attachments#create'

  get 'user_notifications' => 'notifications#user_notifications'
  get 'mark_as_readed' => 'notifications#mark_as_readed'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
