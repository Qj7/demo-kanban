class AddCategoryToIssues < ActiveRecord::Migration
  def change
    add_reference :issues, :category, index: true, foreign_key: true
  end
end
