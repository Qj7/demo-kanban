class CreateAttachments < ActiveRecord::Migration
  def change
    create_table :attachments do |t|
      t.string :document
      t.references :issue
      t.timestamps null: false
    end
  end
end
