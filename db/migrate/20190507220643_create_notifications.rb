class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.string :event
      t.references :user
      t.references :issue
      t.boolean :read, default: false
      t.timestamps null: false
    end
  end
end
