class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.text :body
      t.references :user
      t.references :issue
      t.boolean :read, default: false
      t.timestamps null: false
    end
  end
end
