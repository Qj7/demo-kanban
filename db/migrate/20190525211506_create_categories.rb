class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string :name
      t.integer :rate
      t.integer :permissible_time, :default => 60
      t.timestamps null: false
    end
  end
end
