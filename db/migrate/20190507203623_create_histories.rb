class CreateHistories < ActiveRecord::Migration
  def change
    create_table :histories do |t|
      t.string :event
      t.references :user
      t.references :issue
      t.timestamps null: false
    end
  end
end
