class AddUsernameToUsers < ActiveRecord::Migration
  def change
    add_column :users, :username, :string
    add_column :users, :display_name, :string
    add_index :users, :username

  end
end
