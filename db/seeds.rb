# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
User.create! username: "user", email: 'test@com',
            password: '12345qwer'
User.create! username: "admin", email: 'test2@com', group: 'admin',
            password: '12345qwer'
User.create! username: "tech", email: 'test4@com', group: 'support',
             password: '12345qwer'
User.create! username: "user2", email: "test3@com",
             password: '12345qwer'

Category.create name: 'Интернет'
Category.create name: '1С УТП'
Category.create name: '1С ЗУП'
Category.create name: '1С УАТ'
Category.create name: '1С Прочие'
Category.create name: 'Прочие программы'
Category.create name: 'Картриджи'
Category.create name: 'Оборудование, оргтехника'
Category.create name: 'Доступы, пароли'
Category.create name: 'Звонки Астериск'
Category.create name: 'Телефония'
Category.create name: 'Коммуникация'
Category.create name: 'АРМ ОС Подписка'
Category.create name: 'АРМ ОС Целевые выплаты'
Category.create name: 'АРМ ОС Почтовые услуги'
Category.create name: 'АРМ ОС Кассовые операции'
Category.create name: 'АРМ ОС Торговля'
Category.create name: 'АРМ ОС Феникс'
Category.create name: 'АРМ ОС Коммунальные платежи'
Category.create name: 'АРМ ОС Почтовые переводы'
Category.create name: 'АРМ ОС Электронные платежи'
Category.create name: 'АРМ ОС Технические вопросы'

AdminSetting.create response_time: 15

