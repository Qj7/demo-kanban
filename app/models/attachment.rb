class Attachment < ActiveRecord::Base
  mount_uploader :document, AttachmentUploader
end
