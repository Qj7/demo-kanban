class Comment < ActiveRecord::Base
  has_one :issue
  has_one :user
end
