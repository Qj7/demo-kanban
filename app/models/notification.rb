class Notification < ActiveRecord::Base
  has_one :issue
  has_one :user
end
