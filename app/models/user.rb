class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  mount_uploader :avatar, AvatarUploader
  has_many :owned_issues, :class_name => 'Issue', :foreign_key => 'owner_id'
  has_many :authored_issues, :class_name => 'Issue', :foreign_key => 'author_id'
  belongs_to :history
  belongs_to :comment
  belongs_to :notification


  devise :database_authenticatable,
         :trackable, :validatable

  validates :username, presence: true, uniqueness: true

  before_validation :set_default_group, on: :create
  before_validation :set_default_avatar, on: :create

  private

  def set_default_group
    self.group ||= "user"
  end

  def set_default_avatar
    avatar = [ 'e53935', 'b71c1c', 'd81b60', '880e4f', '8e24aa', '4a148c',
               '5e35b1', '311b92', '3949ab', '1a237e', '1e88e5', '0d47a1', '039be5', '01579b',
               '00acc1', '006064', '00897b', '004d40', '43a047', '1b5e20', '7cb342', '33691e',
               'c0ca33', '827717', 'fdd835', 'f57f17', 'ffb300', 'ff6f00', 'fb8c00', 'e65100',
               'f4511e', 'bf360c', '6d4c41', '3e2723', '757575', '212121', '546e7a', '263238' ]

    a = avatar.sample
    response = HTTParty.get("https://ui-avatars.com/api/?background=#{a}&color=fff&rounded=true&name=#{self.username}+#{self.username}")
    ext = '.' + response.content_type.split('/').last
    tmp = Tempfile.new(['img', ext])
    tmp.binmode
    tmp.write(response.body)
    self.avatar = tmp
  end

end
