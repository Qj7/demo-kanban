class Issue < ActiveRecord::Base
  belongs_to :owner, :class_name => 'User'
  belongs_to :author, :class_name => 'User'
  has_many :histories
  belongs_to :category
  has_many :comments
  has_many :notifications

end
