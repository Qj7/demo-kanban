module ApplicationHelper

  def issues_in_work(issues)
    issues.where(status: 'В работе')
  end

  def issues_in_later(issues)
    issues.where(status: 'Отложена')
  end

  def issues_in_done(issues)
    issues.where(status: 'Выполнена')
  end

  def issues_in_cancel(issues)
    issues.where(status: 'Отменена')
  end

  def find_user_by_id(user_id)
    User.where(id: user_id).first
  end

  def get_done_time(issue)
    issue.histories.where(event: 'Выполнена').first.created_at
  end

  def get_work_time(issue)
    issue.histories.where(event: 'Взята в работу').first.created_at
  end

  def check_delayed(issue)
    events = History.all.where(issue_id: issue.id, event: 'Отложена')
    inwork = 0
    if events
      events.each do |ev|
        delayed_start = ev.created_at
        p ev
        p previous_event = History.all.where(issue_id: issue.id, id: ev.id+1).last
        inwork += previous_event.created_at - delayed_start
      end
    end
    p inwork
  end

  def time_conversion(minutes)
    hours = minutes / 60
    rest = minutes % 60
    "#{hours}:#{rest}"
  end


end
