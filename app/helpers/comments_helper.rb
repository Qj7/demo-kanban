module CommentsHelper
  def self_or_other(comment)
    comment.user_id == current_user.id ? "self" : "other"
  end
end