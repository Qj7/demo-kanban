class CommentsController < ApplicationController
  def create_comment

    @issue = Issue.find(params["issue_id"])
=begin
    @participants = []
    @participants << issue.owner
    @participants << issue.author
=end
    Comment.create comment_params
    @comments = Comment.where(issue_id: @issue.id)
    @author = @issue.author.id
    @owner = @issue.owner.id if @issue.owner
    respond_to do |format|
      format.js
    end
  end

  def count_specific_unread
    issue = Issue.find(params["issue_id"])
    counter = 0
    counter += issue.comments.where.not(user_id: current_user.id).where(read: false).count
    render :json => {comments: {"counter":counter}}
  end

  def count_unread
    issues = Issue.where("issues.author_id = #{current_user.id} OR issues.owner_id= #{current_user.id}")
    counter = 0
    showing = issues.try(:each) do |issue|
       counter += issue.comments.where.not(user_id: current_user.id).where(read: false).count
    end

    render :json => {comments: {"counter":counter, "comments":showing}}

  end

  def readed
    comments = Issue.find(params["issue_id"]).comments
    comments.where.not(user_id: current_user.id).update_all(read: true)
    render :nothing => true, :status => 200, :content_type => 'text/html'
  end

  private
  def comment_params
    params.permit(:body, :issue_id).merge(user_id: current_user.id)
  end
end