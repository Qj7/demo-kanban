class NotificationsController < ApplicationController
  def user_notifications
    notifications = Notification.where(user_id: params['user_id'], read: false)
    render :json => {notifications: {"msg": notifications}}
  end

  def mark_as_readed(user_id)
    notifications = Notification.where(user_id: params['user_id'], read: false)
    notifications.update_all(read: true)
  end
end