class AdminsController < ApplicationController
  def index

  end

  def response_time
    @current = AdminSetting.first.response_time.to_s
  end

  def set_response_time
    p params
    AdminSetting.first.update(response_time: params['minutes'])
    redirect_to admins_response_time_path
  end

  def categories
    @categories = Category.all.order('created_at DESC').reverse
  end
end