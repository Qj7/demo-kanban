class UsersController < ApplicationController

  def root
=begin
    redirect_to users_main_path
=end
  end

  def index
    @my_issues = Issue.where(author: current_user).reverse
    @categories = Category.all.order('created_at DESC').reverse
  end

  def all
    @users = User.all
  end

  def show

  end

  def update
    p params
    current_user.update(avatar: params[:file])
  end

  def online?
    p params
  end

  private

  def user_params
    params.require(:user).permit(:avatar)
  end
end

