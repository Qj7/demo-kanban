class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :custom_authentication
  before_action :authenticate_user!


  protected

  def heh
    @issues = Issues.all
    @new_issues = Issues.where(status: 'Новая').count
  end

  def custom_authentication
=begin
    keytab_path = Rails.root.join('app', 'redmine.keytab')
    kt = KerberosAuthenticator::Krb5::Keytab.new_with_name("FILE:#{keytab_path}")
    user = KerberosAuthenticator::Krb5::Principal.new_with_name(username)
    creds = user.initial_creds_with_password(password, service)
    server_princ = '10.204.4.3:88'
    creds.verify!(server_princ, kt)
=end
    if request.headers['HTTP_X_GSS_USER']
      displayname = []
      ldap = Net::LDAP.new :host => '10.204.4.2',
                           :port => 389
      ldap.search(
          base:         "o=Post Donbass,dc=post,dc=msdnr,dc=ru",
          filter:       Net::LDAP::Filter.eq("uid", 'dolgiy_vv'),

          return_result:true
      ) do |entry|
        if entry.displayname
           displayname << entry.displayname.join('')
        end
        if entry.ou
          p entry.ou.join('')
        end
      end

      user = User.where(username: headers['HTTP_X_GSS_USER'])
      if user.any?
        sign_in(:user, user)
        p current_user.username
        redirect_to root_path
      else
        user = User.new(username: headers['HTTP_X_GSS_USER'], displayname: displayname)
        sign_in(:user, user)
        redirect_to root_path
      end

    end



  end

  def after_sign_in_path_for(resource_or_scope)
    if current_user.group == "user"
      users_main_path
    else
      supports_main_path
    end
  end




end
