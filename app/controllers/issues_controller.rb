class IssuesController < ApplicationController

  def create
    @issue = Issue.create issue_params
    issue_create_history(@issue.author.id, @issue.id)
=begin
    if params[:file]
      create_attachment(params[:file], @issue.id)
    end
=end
    @supports = get_supports
    @issues = Issue.all
    @response_time = AdminSetting.first.response_time
    @my_issues = Issue.where(author_id: current_user.id)
    respond_to do |format|
      format.js
    end

  end

  def show
    @issue = Issue.find(params[:id])
    @comments = Comment.where(issue_id: @issue.id)
  end

  def to_work
    @issue = Issue.find(params[:id])
    @issue.update(owner: current_user, status: 'В работе', open_at: Time.now)
    issue_get_to_work_history(current_user.id, @issue.id)
    create_notification_to_owner(@issue.id)
    create_notification_to_author(@issue.id)
    @response_time = AdminSetting.first.response_time
    @issues = Issue.all
    @recepients = [@issue.author.id, @issue.owner.id]
    @supports = get_supports
    respond_to do |format|
      format.js
    end
  end

  private

  def issue_create_history(user_id, issue_id)
    History.create(event: 'Cоздана', user_id: user_id, issue_id: issue_id)
  end

  def issue_get_to_work_history(user_id, issue_id)
    History.create(event: 'Взята в работу', user_id: user_id, issue_id: issue_id)
  end

  def create_notification_to_owner(issue_id)
    issue = Issue.find(issue_id)
    Notification.create(event: "Заявка №#{issue.id} взята в работу", user_id: issue.owner.id, issue_id: issue.id)
  end

  def create_notification_to_author(issue_id)
    issue = Issue.find(issue_id)
    Notification.create(event: "Заявка №#{issue.id} взята в работу", user_id: issue.author.id, issue_id: issue.id)
  end

  def get_supports
    test = User.where(group: ['support', 'admin'])
    test.ids
  end

  def issue_params
    params.permit(:subject, :body, :category_id).merge(author: current_user, status: 'Новая', priority: 'Обычный')
  end

  def create_attachment(file, issue_id)
    p params
    @icon = Attachment.new(document: file, issue_id: issue_id)
    if @icon.save!
      respond_to do |format|
        format.json{ render :json => @icon }
      end
    end
  end

end