class SupportsController < ApplicationController
  def index
    @issues = Issue.all
    @response_time = AdminSetting.first.response_time
  end

  def dashboard

  end

  def test

  end

  def comments
    @issues = Issue.where(owner_id: current_user.id)
  end

  def find_issue
    issue = Issue.find(params[:id])
    open_at = issue.open_at.to_f * 1000
    pt = issue.category.permissible_time
    render :json => {issue: {"open_at":open_at, "pt":pt}}
  end

  def issues
    @issues = Issue.where(owner_id: current_user.id)
  end

  def update_issue_status
    @issue = Issue.find(params["issue_id"])
    if params["status"] == 'Отложена'
      to_wait(@issue)
      issue_to_wait_history(@issue.owner.id, @issue.id)
    elsif params["status"] == 'В работе'
      to_work(@issue)
      issue_to_work_history(@issue.owner.id, @issue.id)
    elsif params["status"] == 'Выполнена'
      to_done(@issue)
      issue_to_done_history(@issue.owner.id, @issue.id)
    end
    @issues = Issue.where(owner_id: current_user.id)
    respond_to do |format|
      format.js
    end
  end

  def to_cancel
    issue = find(["issue_id"])
    issue_to_cancel_history(issue.id, issue.owner.id)
    issue.update(status: 'Новая', owner_id: nil)
  end

  def update_issue_priority
    @issue = Issue.find(params["issue_id"])
    @issue.update(priority: params["priority"])
    issue_change_priority_history(@issue.owner.id, @issue.id, params['priority'])
    @issues = Issue.where(owner_id: current_user.id)
    respond_to do |format|
      format.js
    end
  end

  def update_issue_classification
    @issue = Issue.find(params["issue_id"])
    @issue.update(classification: params["classification"])
    issue_change_cs_history(@issue.owner.id, @issue.id, params["classification"])
    @issues = Issue.where(owner_id: current_user.id)
    respond_to do |format|
      format.js
    end
  end

  def show_panel
    @issue = Issue.find(params["issue_id"])
    respond_to do |format|
      format.js
    end
  end

  def show_comment
    @issue = Issue.find(params["issue_id"])
    respond_to do |format|
      format.js
    end
  end

  private

  def to_wait(issue)
    issue.update(status: 'Отложена')
  end

  def to_work(issue)
    issue.update(status: 'В работе')
  end

  def to_done(issue)
    issue.update(status: 'Выполнена')
  end

  def issue_to_wait_history(user_id, issue_id)
    History.create(event: 'Отложена', user_id: user_id, issue_id: issue_id)
  end

  def issue_to_work_history(user_id, issue_id)
    History.create(event: 'Взята в работу', user_id: user_id, issue_id: issue_id)
  end

  def issue_to_done_history(user_id, issue_id)
    History.create(event: 'Выполнена', user_id: user_id, issue_id: issue_id)

    events = History.all.where(issue_id: issue_id)

    inwork = 0
    delayed = 0
    delayed_start = nil
    inwork_start = nil

    events.each do |ev|
      if ev.event == 'Взята в работу'
        inwork_start = ev.created_at
        if delayed_start
          delayed += ev.created_at - delayed_start
          delayed_start = nil
        end
      end
      if ev.event == 'Отложена'
        delayed_start = ev.created_at
        if inwork_start
          inwork += ev.created_at - inwork_start
          inwork_start = nil
        end
      end
      if ev.event == 'Выполнена'
        if inwork_start
          inwork += ev.created_at - inwork_start
          inwork_start = nil
        end
        if delayed_start
          delayed += ev.created_at - delayed_start
          delayed_start = nil
        end
      end
    end
    p '-------'
    p 'в секундах'
    p delayed
    p inwork

  end

  def issue_to_cancel_history(user_id, issue_id)
    History.create(event: 'Отменена', user_id: user_id, issue_id: issue_id)
  end

  def issue_change_priority_history(user_id, issue_id, priority)
    History.create(event: "Приоритет изменен #{priority}", user_id: user_id, issue_id: issue_id)
  end

  def issue_change_cs_history(user_id, issue_id, classification)
    History.create(event: "Тип изменен #{classification}", user_id: user_id, issue_id: issue_id)
  end

end