class CategoriesController < ApplicationController
  def create
    time_arr = params['per_time'].split(':')
    time = (time_arr[0].to_i) * 60 + (time_arr[1].to_i)
    cs_params = categories_params.merge(permissible_time: time)
    Category.create cs_params
    redirect_to admins_categories_path
  end

  def update
    time_arr = params['per_time'].split(':')
    time = (time_arr[0].to_i) * 60 + (time_arr[1].to_i)
    cs_params = categories_params.reject { |k, v| v.blank? }.merge(permissible_time: time)
    cs = Category.find(params[:id])
    cs.update cs_params
    redirect_to admins_categories_path
  end

  def destroy
    cs = Category.find(params[:id])
    cs.destroy!
    redirect_to admins_categories_path
  end

  private

  def categories_params
    params.permit(:name, :rate, :permissible_time)
  end
end